#ifndef SINTATICO_H
#define SINTATICO_H

#include "listalex.h"
#include "arvgenerica.h"

#define PROGRAMA 50
#define CORPO 51
#define DECLARACOES 52
#define DEF_FUNCOES 53
#define DEFINICOES 54
#define DEF_VARIAVEIS 55
#define DEF_ESTATICAS 56
#define DEF_TIPOS 57
#define CONSTANTES 58
#define CONSTANTE 59
#define CONST_VALOR 60
#define TIPOS 61
#define VARIAVEIS 62
#define VARIAVEL 63
#define LISTA_VAR 64
#define TIPO_DADO 65
#define FUNCOES 66
#define FUNCAO 67
#define NOME_FUNCAO 68
#define BLOCO_FUNCAO 69
#define BLOCO 70
#define COMANDOS 71
#define COMANDO 72
#define VALOR 73
#define PARAMETROS 74
#define LISTA_PARAM 75
#define EXP_LOGICA 76
#define EXP_MATEMATICA 77
#define NOME_NUMERO 78
#define NOME 79

arvore* sintatico(listatoken *l);



#endif
#include "tabelasimbolos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



TabSimb *insereSimb(TabSimb *l, char *nomeProg, char *tipoVar, char *nomeFun, char *retorno, char *qtdParam, char *nomeVar, char *ordemVar) {

    TabSimb *p = (TabSimb *) malloc(sizeof (TabSimb));
    p->nomeProg = (char*)malloc(30 * sizeof (char));
    p->tipoVar = (char*)malloc(30 * sizeof (char));
    p->nomeFun = (char*)malloc(30 * sizeof (char));
    p->retorno = (char*)malloc(30 * sizeof (char));
    p->qtdParam = (char*)malloc(30 * sizeof (char));
    p->nomeVar = (char*)malloc(30 * sizeof (char));
    p->ordemVar = (char*)malloc(30 * sizeof (char));
    
    strcpy(p->nomeProg, nomeProg);
    strcpy(p->tipoVar, tipoVar);
    strcpy(p->nomeFun, nomeFun);
    strcpy(p->retorno, retorno);
    strcpy(p->qtdParam, qtdParam);
    strcpy(p->nomeVar, nomeVar);
    strcpy(p->ordemVar, ordemVar);
    if (!l) {
        l = p;
        p->prox = NULL;
    } else {
        TabSimb *q = l;
        while (q->prox) {
            q = q->prox;
        }
        q->prox = p;
        p->prox = NULL;
    }
    return l;
}

void imprimeTabela(TabSimb *l) {
    TabSimb *p = l;
    while (p) {
        
        printf("Nome programa: %s  Tipo variavel: %s  Nome funcao: %s  Retorno: %s  Parametros: %s  Variavel: %s  Ordem variavel: %s", p->nomeProg, p->tipoVar, p->nomeFun, p->retorno, p->qtdParam, p->nomeVar, p->ordemVar);
        printf("\n\n");
        p = p->prox;
    }
}

int contaParametros(TabSimb *tab){

    int cont = 1;
    TabSimb *s = tab;
    while(s){

        cont++;
        s = s->prox;
    }
    return cont-1;
}
#ifndef ARVGENERICA_H
#define ARVGENERICA_H

typedef struct arvgen{
        char *token; //nome da regra
        int linha;
		int lexema; //identificador regra
        struct arvgen *filho, *irmao;
}arvore;

arvore *criaArv(int lexema, char *token, int linha);
void insereNo(arvore *a, arvore *suba);
void imprimeArv(arvore *a, int n);

#endif
#ifndef TABELASIMBOLOS_H
#define TABELASIMBOLOS_H

typedef struct tabsimbolos {
    char *nomeProg, *tipoVar, *nomeFun, *retorno, *qtdParam, *nomeVar, *ordemVar;
    struct tabsimbolos *prox;
} TabSimb;


TabSimb *insereSimb(TabSimb *l, char *nomeProg, char *tipoVar, char *nomeFun, char *retorno, char *qtdParam, char *nomeVar, char *ordemVar);
void imprimeTabela(TabSimb *l);




#endif

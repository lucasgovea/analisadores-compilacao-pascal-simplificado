#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "sintatico.h"
#include "lexico.h"
#include "listalex.h"
#include "arvgenerica.h"



int firstPrograma[]={PROGRAM};
int firstCorpo[]={TYPE,CONST,BEGIN,WHILE,IF,EPSILON,IDENTIFICADOR};
int firstDeclaracoes[]={CONST,TYPE,EPSILON};
int firstDef_Funcoes[]={FUNCTION, EPSILON};
int firstDefinicoes[]={CONST, TYPE, EPSILON};
int firstDef_Variaveis[]={VAR, EPSILON};
int firstDef_Estaticas[]={CONST,TYPE,EPSILON};
int firstDef_Tipos[]={TYPE,EPSILON};
int firstConstantes[]={IDENTIFICADOR, EPSILON};
int firstConstante[]={IDENTIFICADOR} ;
int firstConst_Valor[]={NUMERO, IDENTIFICADOR};
int firstTipos[]={IDENTIFICADOR, EPSILON};
int firstTipo[]={IDENTIFICADOR};
int firstVariaveis[]={IDENTIFICADOR, EPSILON};
int firstVariavel[]={IDENTIFICADOR};
int firstLista_Var[]={IDENTIFICADOR};
int firstTipo_Dado[]={TIPO,IDENTIFICADOR};
int firstFuncoes[]={EPSILON,TIPO, IDENTIFICADOR};
int firstFuncao[]={TIPO, IDENTIFICADOR};
int firstNome_Funcao[]={TIPO, IDENTIFICADOR};
int firstBloco_Funcao[]={VAR,BEGIN,WHILE,IF,IDENTIFICADOR};
int firstBloco[]={BEGIN, WHILE, IF,IDENTIFICADOR};
int firstComandos[]={END,WHILE, IF,IDENTIFICADOR};
int firstComando[]={WHILE, IF,IDENTIFICADOR};
int firstAtribuicao[]={ATRIBUICAO};
int firstValor[]={IDENTIFICADOR, NUMERO}; 
int firstParametros[]={ABRE_PARENTESES};
int firstLista_Param[]={IDENTIFICADOR, NUMERO};
int firstExp_Logica[]={IDENTIFICADOR,NUMERO};
int firstOp_Relacional[]={OP_RELACIONAL, IGUAL};
int firstExp_Matematica[]={IDENTIFICADOR,NUMERO,EPSILON}; //tem epsilon por nao determinismo
int firstOp_Matematico[]={OP_MATEMATICO};
int firstNome_Numero[]={IDENTIFICADOR,NUMERO};
int firstNome[]={IDENTIFICADOR, EPSILON}; //tem epsilon por nao determinismo

int followPrograma[]={END_OF_FILE};
int followCorpo[]={END_OF_FILE};
int followDeclaracoes[]={BEGIN, WHILE, IF,IDENTIFICADOR};
int followDef_Funcoes[]={BEGIN, WHILE, IF,IDENTIFICADOR};
int followDefinicoes[]={EPSILON,FUNCTION};
int followDef_Variaveis[]={EPSILON,FUNCTION};
int followDef_Estaticas[]={EPSILON, VAR};
int followDef_Tipos[]={EPSILON, VAR};
int followConstantes[]={EPSILON,TYPE};
int followConstante[]={PONTO_VIRGULA};
int followConst_Valor[]={PONTO_VIRGULA};
int followTipos[]={EPSILON, VAR};
int followTipo[]={PONTO_VIRGULA};
int followVariaveis[]={EPSILON, FUNCTION,FECHA_PARENTESES,BEGIN, WHILE, IF,IDENTIFICADOR};
int followVariavel[]={PONTO_VIRGULA};
int followLista_Var[]={TIPO,IDENTIFICADOR};
int followTipo_Dado[]={PONTO_VIRGULA,IDENTIFICADOR};
int followFuncoes[]={BEGIN, WHILE, IF,IDENTIFICADOR};
int followFuncao[]={EPSILON,TIPO,IDENTIFICADOR};
int followNome_Funcao[]={VAR,BEGIN,WHILE,IF,IDENTIFICADOR};
int followBloco_Funcao[]={EPSILON,TIPO, IDENTIFICADOR};
int followBloco[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA}; 
int followComandos[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA,IDENTIFICADOR};
int followComando[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA,IDENTIFICADOR};
int followAtribuicao[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA,IDENTIFICADOR};
int followValor[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA,IDENTIFICADOR};
int followParametros[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA,IDENTIFICADOR};
int followLista_Param[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA,IDENTIFICADOR};
int followExp_Logica[]={BEGIN, WHILE, IF,IDENTIFICADOR,THEN};
int followOp_Relacional[]={IDENTIFICADOR,NUMERO};
int followExp_Matematica[]={EPSILON,END_OF_FILE,TIPO,PONTO_VIRGULA,OP_RELACIONAL,BEGIN, WHILE, IF,IDENTIFICADOR,THEN, IGUAL};
int followOp_Matematico[]={IDENTIFICADOR,NUMERO};
int followNome_Numero[]={EPSILON,VIRGULA, FECHA_PARENTESES,OP_MATEMATICO,END_OF_FILE,TIPO,PONTO_VIRGULA,OP_RELACIONAL,BEGIN, WHILE, IF,IDENTIFICADOR,THEN,FECHA_COLCHETE,IGUAL};
int followNome[]={EPSILON,ATRIBUICAO,VIRGULA, FECHA_PARENTESES,OP_MATEMATICO,END_OF_FILE,TIPO,PONTO_VIRGULA,OP_RELACIONAL,BEGIN, WHILE, IF,IDENTIFICADOR,THEN,FECHA_COLCHETE,IGUAL};
int followIdentificador[]={EPSILON, IGUAL, DOIS_PONTOS, VIRGULA, ABRE_PARENTESES, ABRE_COLCHETE,ATRIBUICAO,VIRGULA, FECHA_PARENTESES,OP_MATEMATICO,END_OF_FILE,TIPO,PONTO_VIRGULA,OP_RELACIONAL,BEGIN, WHILE, IF,IDENTIFICADOR,THEN,FECHA_COLCHETE};
int followNumero[]={EPSILON,VIRGULA, FECHA_PARENTESES,OP_MATEMATICO,END_OF_FILE,TIPO,PONTO_VIRGULA,OP_RELACIONAL,BEGIN, WHILE, IF,IDENTIFICADOR,THEN,FECHA_COLCHETE,IGUAL};

arvore* sintatico(listatoken *l);
listatoken *nextToken();
int validaFirst(int lexema, int vetor[],int max);
int validaFollow(int lexema, int vetor[],int max);
arvore* trataPrograma();
void trataIdentificador(arvore *a);
void trataCorpo(arvore *a);
void trataDeclaracoes(arvore *a);
void trataDef_Funcoes(arvore *a);
void trataDefinicoes(arvore *a);
void trataDef_Variaveis(arvore *a);
void trataDef_Estaticas(arvore *a);
void trataDef_Tipos(arvore *a);
void trataConstantes(arvore *a);
void trataConstante(arvore *a);
void trataConst_Valor(arvore *a);
void trataNumero(arvore *a);
void trataTipos(arvore *a);
void trataTipo(arvore *a);
void trataVariaveis(arvore *a);
void trataVariavel(arvore *a);
void trataLista_Var(arvore *a);
void trataTipo_Dado(arvore *a);
void trataFuncoes(arvore *a);
void trataFuncao(arvore *a);
void trataNome_Funcao(arvore *a);
void trataBloco_Funcao(arvore *a);
void trataBloco(arvore *a);
void trataComandos(arvore *a);
void trataComando(arvore *a);
void trataAtribuicao(arvore *a);
void trataValor(arvore *a);
void trataParametros(arvore *a);
void trataLista_Param(arvore *a);
void trataExp_Logica(arvore *a);
void trataExp_Logica(arvore *a);
void trataOp_Relacional(arvore *a);
void trataExp_Matematica(arvore *a);
void trataOp_Matematico(arvore *a);
void trataNome_Numero(arvore *a);
void trataNome(arvore *a);
void trata_erro_sintatico(char *erro,int linha, int follow[], int n);

listatoken *k = NULL;

void trata_erro_sintatico(char *erro, int linha, int follow[], int n) {

    /*char *erro2 = calloc(80, sizeof (char));
    strcpy(erro2, erro);
    push(p, erro, k->linha);
    free(erro);*/
	printf("linha:%d - %s\n", linha, erro);
	do 
	{
        k = nextToken();
    }while ((!validaFollow(k->lexema, follow, n)) && (k->proximo != NULL));
    if (!k->proximo) 
	{
        printf("Fim de arquivo inesperado\n");
		system("pause");
        exit(1);
    }
}

listatoken *nextToken() {

    if (!k->proximo) {
        if (k->lexema==END) {
            printf("Fim de arquivo inesperado\n");
			system("pause");
            exit(1);
        }
		
    }
	//printf("ultimo token tratado: %s    linha:%d\n",k->token, k->linha);
    return k->proximo;
}

arvore* sintatico(listatoken *l)
{
	k=l;
	arvore *a = trataPrograma();
	return a;
}


int validaFirst(int lexema, int vetor[], int max) 
{	
    int i;
    for (i = 0; i < max; i++) 
	{
        if (lexema == vetor[i]) 
		{
            return 1; //retorna 1 se o lexema pertencer ao vetor
        }
    }
    return 0; //retorna 0 se o lexema nao pertencer ao vetor
}

int validaFollow(int lexema, int vetor[], int max) 
{
    int i;

	if(lexema == vetor[0])
	{
		return 1; //caso epsilon esteja no vetor, ele � aceito como follow (o epsilon sempre esta na primeira posicao do conjunto follow)
	}
 
    for (i = 0; i < max; i++) 
	{
        if (lexema == vetor[i])
		{
            return 1;//retorna 1 se o lexema pertencer ao vetor
        }
    }
    return 0;//retorna 0 se o lexema nao pertencer ao vetor
}

arvore* trataPrograma()
{
	arvore *a = criaArv(PROGRAMA, "PROGRAMA", k->linha);
	int lenMax=sizeof(firstPrograma)/sizeof(int);
	if(validaFirst(k->lexema, firstPrograma, lenMax))
	{
		arvore *b= criaArv(k->lexema,k->token, k->linha);
		insereNo(a,b);
		k=nextToken();
		b = criaArv(IDENTIFICADOR, "IDENTIFICADOR", k->linha);
		insereNo(a,b);
		//inserir simbolo na tabela de simbolos aki
		trataIdentificador(b);
		if(k->lexema==PONTO_VIRGULA)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			b = criaArv(CORPO, "CORPO", k->linha);
			insereNo(a,b);
			trataCorpo(b);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'program' esperado");
		lenMax=sizeof(followPrograma)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followPrograma,lenMax);
		free(erro);
	}
	return a;

}
void trataIdentificador(arvore *a)
{
	if (k->lexema==IDENTIFICADOR)
	{
        arvore *b = criaArv(k->lexema, k->token, k->linha);
        insereNo(a, b);
        k = nextToken();
    } 
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' esperado");
		int lenMax=sizeof(followPrograma)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followIdentificador,lenMax);
		free(erro);
	}
	
}
void trataCorpo(arvore *a)
{
	int lenMax=sizeof(firstCorpo)/sizeof(int);
	if(validaFirst(k->lexema, firstCorpo, lenMax))
	{
		lenMax=sizeof(firstBloco)/sizeof(int);
		if(validaFirst(k->lexema,firstBloco, lenMax))
		{
			arvore *b = criaArv(BLOCO, "BLOCO", k->linha);
			insereNo(a,b);
			trataBloco(b);
		}
		else
		{
			arvore *b;//= criaArv(k->lexema,k->token, k->linha);
			//insereNo(a,b);
			//k=nextToken();
			b = criaArv(DECLARACOES, "DECLARACOES", k->linha);
			insereNo(a,b);
			trataDeclaracoes(b);
			if(k)
			{
				arvore *b;//= criaArv(k->lexema,k->token, k->linha);
				//insereNo(a,b);
				//k=nextToken();
				b = criaArv(BLOCO, "BLOCO", k->linha);
				insereNo(a,b);
				trataBloco(b);
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				//exit(1);
			}
		}
	}
	/*else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'const', 'type', 'begin', 'identificador', 'while' ou 'if' esperado");
		lenMax=sizeof(followCorpo)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followCorpo,lenMax);
		free(erro);
	}*/
}
void trataDeclaracoes(arvore *a)
{
	int lenMax=sizeof(firstDeclaracoes)/sizeof(int);
	if(validaFirst(k->lexema, firstDeclaracoes, lenMax))
	{
		arvore *b = criaArv(DEFINICOES, "DEFINICOES", k->linha);
		insereNo(a,b);
		trataDefinicoes(b);
		if(k)
		{
			arvore *b = criaArv(DEF_FUNCOES, "DEF_FUNCOES", k->linha);
			insereNo(a,b);
			trataDef_Funcoes(b);
		}
		else
		{
			printf("Fim de arquivo inesperado\n");
			exit(1);
		}
	}
	/*else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'const' ou 'type' esperado");
		lenMax=sizeof(followDeclaracoes)/sizeof(int);
        trata_erro_sintatico(erro,k->linha,followDeclaracoes,lenMax);
		free(erro);
	}*/
}
void trataDef_Funcoes(arvore *a)
{
	int lenMax=sizeof(firstDef_Funcoes)/sizeof(int);
	if(validaFirst(k->lexema,firstDef_Funcoes,lenMax))
	{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			b = criaArv(FUNCOES, "FUNCOES", k->linha);
			insereNo(a,b);
			trataFuncoes(b);			
	}
	//nao tem else pois pode ser epsilon na regra
}
void trataDefinicoes(arvore *a)
{
	int lenMax=sizeof(firstDefinicoes)/sizeof(int);
	if(validaFirst(k->lexema,firstDefinicoes,lenMax))
	{
		arvore *b = criaArv(DEF_ESTATICAS, "DEF_ESTATICAS", k->linha);
		insereNo(a,b);
		trataDef_Estaticas(b);
		if(k)
		{
			arvore *b = criaArv(DEF_VARIAVEIS, "DEF_VARIAVEIS", k->linha);
			insereNo(a,b);
			trataDef_Variaveis(b);
		}
		else
		{
			printf("Fim de arquivo inesperado\n");
			exit(1);
		}
	}
	/*else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'const' ou 'type' esperado");
		lenMax=sizeof(followDeclaracoes)/sizeof(int);
        trata_erro_sintatico(erro,k->linha,followDeclaracoes,lenMax);
		free(erro);
	}*/
}
void trataDef_Variaveis(arvore *a)
{
	int lenMax=sizeof(firstDef_Variaveis)/sizeof(int);
	if(validaFirst(k->lexema,firstDef_Variaveis,lenMax))
	{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			b = criaArv(VARIAVEIS, "VARIAVEIS", k->linha);
			insereNo(a,b);
			trataVariaveis(b);			
	}
	//nao tem else pois pode ser epsilon na regra
}

void trataDef_Estaticas(arvore *a)
{
	int lenMax=sizeof(firstDef_Estaticas)/sizeof(int);
	if(validaFirst(k->lexema,firstDef_Estaticas, lenMax))
	{
		lenMax=sizeof(firstDef_Tipos)/sizeof(int);
		if(validaFirst(k->lexema,firstDef_Tipos,lenMax))
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			b = criaArv(DEF_TIPOS, "DEF_TIPOS", k->linha);
			insereNo(a,b);
			trataDef_Tipos(b);
		}
		else
		{
			
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			b = criaArv(CONSTANTES, "CONSTANTES", k->linha);
			insereNo(a,b);
			trataConstantes(b);
			if(k)
			{
				//arvore *b= criaArv(k->lexema,k->token, k->linha);
				//insereNo(a,b);
				b = criaArv(DEF_TIPOS, "DEF_TIPOS", k->linha);
				insereNo(a,b);
				trataDef_Tipos(b);
			}
			else
			{
				printf("Fim de arquivo inesperado\n");
				exit(1);
			}
		}
	}
	/*else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'const' ou 'type' esperado");
		lenMax=sizeof(followDef_Estaticas)/sizeof(int);
        trata_erro_sintatico(erro,k->linha,followDef_Estaticas,lenMax);
		free(erro);
	}*/
}
void trataDef_Tipos(arvore *a)
{
	int lenMax=sizeof(firstDef_Tipos)/sizeof(int);
	if(validaFirst(k->lexema,firstDef_Tipos,lenMax))
	{
		arvore *b= criaArv(k->lexema,k->token, k->linha);
		insereNo(a,b);
		k=nextToken();
		b = criaArv(TIPOS, "TIPOS", k->linha);
		insereNo(a,b);
		trataTipos(b);			
	}
	//nao tem else pois pode ser epsilon na regra
}
void trataConstantes(arvore *a)
{
	int lenMax=sizeof(firstConstantes)/sizeof(int);
	if(validaFirst(k->lexema,firstConstantes,lenMax))
	{
		arvore *b = criaArv(CONSTANTE, "CONSTANTE", k->linha);
		insereNo(a,b);
		trataConstante(b);
		if(k->lexema==PONTO_VIRGULA)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			if (k)
			{
				b = criaArv(CONSTANTES, "CONSTANTES", k->linha);
				insereNo(a, b);
				trataConstantes(b);
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
		else
		{
			char *erro=(char*)malloc(sizeof(char)*80);
			strcpy(erro,"';' esperado.");
			int lenMax=sizeof(followConstantes)/sizeof(int);
			trata_erro_sintatico(erro,k->linha,followConstantes,lenMax);
			free(erro);
		}
	}
	//nao tem else pois pode ser epsilon na regra
}
void trataConstante(arvore *a)
{
	int lenMax=sizeof(firstConstante)/sizeof(int);
	if(validaFirst(k->lexema,firstConstante,lenMax))
	{
		
		arvore *b = criaArv(IDENTIFICADOR, "IDENTIFICADOR", k->linha);
		insereNo(a,b);
		trataIdentificador(b);
		if(k->lexema == IGUAL) 
		{
			b = criaArv(k->lexema, k->token, k->linha);
            insereNo(a, b);
            k = nextToken();
			if(k)
			{
				arvore *b = criaArv(CONST_VALOR, "CONST_VALOR", k->linha);
				insereNo(a,b);
				trataConst_Valor(b);
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				
			}
		}
		else
		{
			char *erro=(char*)malloc(sizeof(char)*80);
			strcpy(erro,"'=' esperado.");
			int lenMax=sizeof(followConstante)/sizeof(int);
			trata_erro_sintatico(erro,k->linha,followConstante,1);
			free(erro);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' esperado");
		lenMax=sizeof(followConstante)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followConstante,lenMax);
		free(erro);
	}

}
void trataConst_Valor(arvore *a)
{
	int lenMax=sizeof(firstConst_Valor)/sizeof(int);
	if(validaFirst(k->lexema,firstConst_Valor,lenMax))
	{
		if (k->lexema==IDENTIFICADOR)
		{
			arvore *b = criaArv(k->lexema, k->token, k->linha);
			insereNo(a,b);
			k = nextToken();
		}
		else
		{
			arvore *b = criaArv(EXP_MATEMATICA, "EXP_MATEMATICA", k->linha);
			insereNo(a,b);
			trataExp_Matematica(b);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' ou 'numero' esperado");
		lenMax=sizeof(followConst_Valor)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followConst_Valor,lenMax);
		free(erro);
	}
}
void trataNumero(arvore *a)
{
	if (k->lexema==NUMERO)
	{
        arvore *b = criaArv(k->lexema, k->token, k->linha);
        insereNo(a, b);
        k = nextToken();
    }
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'numero' esperado");
		int lenMax=sizeof(followConst_Valor)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followNumero,lenMax);
		free(erro);
	}
}
void trataTipos(arvore *a)
{
	int lenMax=sizeof(firstTipos)/sizeof(int);
	if(validaFirst(k->lexema,firstTipos,lenMax))
	{
		arvore *b = criaArv(TIPO, "TIPO", k->linha);
		insereNo(a,b);
		trataTipo(b);
		if(k->lexema==PONTO_VIRGULA)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			if (k) 
			{
				b = criaArv(TIPOS, "TIPOS", k->linha);
				insereNo(a, b);
				trataTipos(b);
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
		else
		{
			char *erro=(char*)malloc(sizeof(char)*80);
			strcpy(erro,"';' esperado");
			lenMax=sizeof(followTipos)/sizeof(int);
			trata_erro_sintatico(erro,k->linha,followTipos,lenMax);
			free(erro);
		}
	}
	//nao tem else pois pode ser epsilon na regra
}
void trataTipo(arvore *a)
{
	int lenMax=sizeof(firstTipo)/sizeof(int);
	if(validaFirst(k->lexema,firstTipo,lenMax))
	{
		
		arvore *b = criaArv(IDENTIFICADOR, "IDENTIFICADOR", k->linha);
		insereNo(a,b);
		trataIdentificador(b);
		if(k->lexema== IGUAL) 
		{
			b = criaArv(k->lexema, k->token, k->linha);
            insereNo(a, b);
            k = nextToken();
			if(k)
			{
				arvore *b = criaArv(TIPO_DADO, "TIPO_DADO", k->linha);
				insereNo(a,b);
				trataTipo_Dado(b);
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
		else
		{
			char *erro=(char*)malloc(sizeof(char)*80);
			strcpy(erro,"'=' esperado.");
			lenMax=sizeof(followTipo)/sizeof(int);
			trata_erro_sintatico(erro,k->linha,followTipo,lenMax);
			free(erro);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' esperado.");
		lenMax=sizeof(followTipo)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followTipo,lenMax);
		free(erro);
	}
}
void trataVariaveis(arvore *a)
{
	int lenMax=sizeof(firstVariaveis)/sizeof(int);
	if(validaFirst(k->lexema,firstVariaveis,lenMax))
	{
		arvore *b = criaArv(VARIAVEL, "VARIAVEL", k->linha);
		insereNo(a,b);
		trataVariavel(b);
		if(k->lexema==PONTO_VIRGULA)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			if (k) 
			{
				b = criaArv(VARIAVEIS, "VARIAVEIS", k->linha);
				insereNo(a, b);
				trataVariaveis(b);
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
		else
		{
			char *erro=(char*)malloc(sizeof(char)*80);
			strcpy(erro,"';' esperado.");
			lenMax=sizeof(followVariaveis)/sizeof(int);
			trata_erro_sintatico(erro,k->linha,followVariaveis,lenMax);
			free(erro);
		}
	}
	//nao tem else pois pode ser epsilon na regra
}
void trataVariavel(arvore *a)
{
	int lenMax=sizeof(firstVariavel)/sizeof(int);
	if(validaFirst(k->lexema, firstVariavel,lenMax))
	{
		arvore *b = criaArv(LISTA_VAR, "LISTA_VAR", k->linha);
		insereNo(a,b);
		trataLista_Var(b);
		if(k)
		{
			//arvore *b= criaArv(k->lexema,k->token, k->linha);
			//insereNo(a,b);
			b = criaArv(TIPO_DADO, "TIPO_DADO", k->linha);
			insereNo(a,b);
			trataTipo_Dado(b);
		}
		else
		{
			printf("Fim de arquivo inesperado\n");
			exit(1);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' esperado.");
		lenMax=sizeof(followVariavel)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followVariavel,lenMax);
		free(erro);
	}
}
void trataLista_Var(arvore *a)
{
	int lenMax=sizeof(firstLista_Var)/sizeof(int);
	if(validaFirst(k->lexema,firstLista_Var, lenMax))
	{
		arvore *b = criaArv(IDENTIFICADOR, "IDENTIFICADOR", k->linha);
		insereNo(a,b);
		trataIdentificador(b);
		if (k) 
		{
			if(k->lexema== VIRGULA)
			{
				arvore *b= criaArv(k->lexema,k->token, k->linha);
				insereNo(a,b);
				k=nextToken();
				b = criaArv(LISTA_VAR, "LISTA_VAR", k->linha);
				insereNo(a,b);
				trataLista_Var(b);					
			}
			else
			{
				if(k->lexema== DOIS_PONTOS)
				{
					arvore *b= criaArv(k->lexema,k->token, k->linha);
					insereNo(a,b);
					k=nextToken();
				}
				else
				{
					char *erro=(char*)malloc(sizeof(char)*80);
					strcpy(erro,"',' ou ':' esperado.");
					int DOIS_PONTOS_VIRGULA[]={VIRGULA, DOIS_PONTOS};
					lenMax=(sizeof(DOIS_PONTOS_VIRGULA)/sizeof(int));
					trata_erro_sintatico(erro,k->linha,DOIS_PONTOS_VIRGULA,lenMax);
					free(erro);
				}
			}
		}	
		else
		{
			printf("Fim de arquivo inesperado!\n");
			exit(1);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' esperado.");
		lenMax=sizeof(followLista_Var)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followLista_Var,lenMax);
		free(erro);
	}
}
void trataTipo_Dado(arvore *a)
{
	int lenMax=sizeof(firstTipo_Dado)/sizeof(int);
	if(validaFirst(k->lexema,firstTipo_Dado,lenMax))
	{
		if(k->lexema=TIPO)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			if(k)
			{
				if(k->lexema==ABRE_COLCHETE)
				{
					arvore *b= criaArv(k->lexema,k->token, k->linha);
					insereNo(a,b);
					k=nextToken();
					b = criaArv(NUMERO, "NUMERO", k->linha);
					insereNo(a,b);
					trataNumero(b);
					if(k)
					{
						if(k->lexema==FECHA_COLCHETE)
						{
							arvore *b= criaArv(k->lexema,k->token, k->linha);
							insereNo(a,b);
							k=nextToken();
							if(k)
							{
								if(k->lexema==OF)
								{
									arvore *b= criaArv(k->lexema,k->token, k->linha);
									insereNo(a,b);
									k=nextToken();
									b = criaArv(TIPO_DADO, "TIPO_DADO", k->linha);
									insereNo(a,b);
									trataTipo_Dado(b);
								}
								else
								{
									char *erro=(char*)malloc(sizeof(char)*80);
									strcpy(erro,"'of' esperado.");
									lenMax=sizeof(followTipo_Dado)/sizeof(int);
									trata_erro_sintatico(erro,k->linha,followTipo_Dado,lenMax);
									free(erro);
								}
							}
							else
							{
								printf("Fim de arquivo inesperado.\n");
								exit(1);
							}
						}
						else
						{
							char *erro=(char*)malloc(sizeof(char)*80);
							strcpy(erro,"']' esperado.");
							lenMax=sizeof(followTipo_Dado)/sizeof(int);
							trata_erro_sintatico(erro,k->linha,followTipo_Dado,lenMax);
							free(erro);
						}
					}
					else
					{
						printf("Fim de arquivo inesperado.\n");
						exit(1);
					}

				}
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'tipo' ou 'identificador' esperado.");
		lenMax=sizeof(followTipo_Dado)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followTipo_Dado,lenMax);
		free(erro);
	}
}

void trataFuncoes(arvore *a)
{
	int lenMax=sizeof(firstFuncoes)/sizeof(int);
	if(validaFirst(k->lexema, firstFuncoes,lenMax))
	{
			arvore *b = criaArv(FUNCAO, "FUNCAO", k->linha);
			insereNo(a,b);
			trataFuncao(b);
			if(k)
			{
				//arvore *b= criaArv(k->lexema,k->token, k->linha);
				//insereNo(a,b);
				b = criaArv(FUNCOES, "FUNCOES", k->linha);
				insereNo(a,b);
				trataFuncoes(b);
			}
			else
			{
				printf("Fim de arquivo inexperado\n!");
				exit(1);
			}
			
	}
	//nao tem else pois pode ser epsilon na regra
}
void trataFuncao(arvore *a)
{
	int lenMax=sizeof(firstFuncao)/sizeof(int);
	if(validaFirst(k->lexema,firstFuncao,lenMax))
	{
		arvore *b = criaArv(NOME_FUNCAO, "NOME_FUNCAO", k->linha);
		insereNo(a,b);
		trataNome_Funcao(b);
		if(k)
		{	
			b = criaArv(BLOCO_FUNCAO, "BLOCO_FUNCAO", k->linha);
			insereNo(a,b);
			//arvore *b= criaArv(k->lexema,k->token, k->linha);
			//insereNo(a,b);
			//k=nextToken();
			trataBloco_Funcao(b);
		}
		else
		{
			printf("Fim de arquivo inesperado\n");
			exit(1);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'tipo' ou 'identificador' esperado.");
		lenMax=sizeof(followFuncao)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followFuncao,lenMax);
		free(erro);
	}
}
void trataNome_Funcao(arvore *a)
{
	int lenMax=sizeof(firstNome_Funcao)/sizeof(int);
	if(validaFirst(k->lexema,firstNome_Funcao, lenMax))
	{
		arvore *b = criaArv(TIPO_DADO, "TIPO_DADO", k->linha);
		insereNo(a,b);
		trataTipo_Dado(b);
		if(k)
		{
			//arvore *b= criaArv(k->lexema,k->token, k->linha);
			//insereNo(a,b);
			b = criaArv(IDENTIFICADOR, "IDENTIFICADOR", k->linha);
			insereNo(a,b);
			trataIdentificador(b);
			if(k)
			{
				if(k->lexema==ABRE_PARENTESES)
				{
					arvore *b= criaArv(k->lexema,k->token, k->linha);
					insereNo(a,b);
					k=nextToken();
					if(k)
					{
						//arvore *b= criaArv(k->lexema,k->token, k->linha);
						//insereNo(a,b);
						b = criaArv(VARIAVEIS, "VARIAVEIS", k->linha);
						insereNo(a,b);
						trataVariaveis(b);
						//k=nextToken();
						if(k)
						{
							if(k->lexema==FECHA_PARENTESES)
							{
								arvore *b= criaArv(k->lexema,k->token, k->linha);
								insereNo(a,b);
								k=nextToken();
								
							}
							else
							{
								char *erro=(char*)malloc(sizeof(char)*80);
								strcpy(erro,"')' esperado.");
								lenMax=sizeof(followNome_Funcao)/sizeof(int);
								trata_erro_sintatico(erro,k->linha,followNome_Funcao,lenMax);
								free(erro);
							}

						}
						else
						{
							printf("Fim de arquivo inesperado\n");
							exit(1);
						}
					}
					else
					{
						printf("Fim de arquivo inesperado\n");
						exit(1);
					}
				}
				else
				{
					char *erro=(char*)malloc(sizeof(char)*80);
					strcpy(erro,"'(' esperado.");
					lenMax=sizeof(followNome_Funcao)/sizeof(int);
					trata_erro_sintatico(erro,k->linha,followNome_Funcao,lenMax);
					free(erro);
				}
			}
			else
			{
				printf("Fim de arquivo inesperado\n");
				exit(1);
			}
		}
		else
		{
			printf("Fim de arquivo inesperado\n");
			exit(1);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'tipo' ou 'identificador' esperado.");
		lenMax=sizeof(followNome_Funcao)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followNome_Funcao,lenMax);
		free(erro);
	}
}
void trataBloco_Funcao(arvore *a)
{
	int lenMax=sizeof(firstBloco_Funcao)/sizeof(int);
	if(validaFirst(k->lexema,firstBloco_Funcao, lenMax))
	{
		lenMax=sizeof(firstBloco)/sizeof(int);
		if(validaFirst(k->lexema,firstBloco, lenMax))
		{
			arvore *b;
			b = criaArv(BLOCO, "BLOCO", k->linha);
			insereNo(a,b);
			trataBloco(b);
		}
		else
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken(); //botei dps
			b = criaArv(VARIAVEIS, "VARIAVEIS", k->linha);
			insereNo(a,b);
			trataVariaveis(b);
			if(k)
			{
				//b= criaArv(k->lexema,k->token, k->linha);
				//insereNo(a,b);
				b = criaArv(BLOCO, "BLOCO", k->linha);
				insereNo(a,b);
				trataBloco(b);
			}
			else
			{
				printf("Fim de arquivo inesperado\n");
				exit(1);
			}
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'var', 'begin', 'identificador', 'while' ou 'if' esperado.");
		lenMax=sizeof(followBloco_Funcao)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followBloco_Funcao,lenMax);
		free(erro);
	}

}
void trataBloco(arvore *a)
{
	int lenMax=sizeof(firstBloco)/sizeof(int);
	if(validaFirst(k->lexema,firstBloco,lenMax))
	{
		lenMax=sizeof(firstComando)/sizeof(int);
		if(validaFirst(k->lexema,firstComando, lenMax))
		{
			arvore *b;//= criaArv(k->lexema,k->token, k->linha);
			//insereNo(a,b);
			
			b = criaArv(COMANDO, "COMANDO", k->linha);
			insereNo(a,b);
			trataComando(b);
		}
		else
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
			b = criaArv(COMANDOS, "COMANDOS", k->linha);
			insereNo(a,b);
			trataComandos(b);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'begin','identificador', 'while' ou 'if' esperado.");
		lenMax=sizeof(followBloco)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followBloco,lenMax);
		free(erro);
	}
}
void trataComandos(arvore *a)
{
	int lenMax=sizeof(firstComandos)/sizeof(int);
	if(validaFirst(k->lexema,firstComandos,lenMax))
	{
		if(k->lexema==END)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();
		}
		else
		{
			arvore *b = criaArv(COMANDO, "COMANDO", k->linha);
			insereNo(a,b);
			trataComando(b);
			//k=nextToken();
			if (k) 
			{
				if(k->lexema== PONTO_VIRGULA)
				{
					b= criaArv(k->lexema,k->token, k->linha);
					insereNo(a,b);
					b = criaArv(COMANDOS, "COMANDOS", k->linha);
					insereNo(a,b);
					k=nextToken();//botei dps
					trataComandos(b);
					
				}
				else
				{
					char *erro=(char*)malloc(sizeof(char)*80);
					strcpy(erro,"';' esperado.");
					lenMax=sizeof(followComandos)/sizeof(int);
					trata_erro_sintatico(erro,k->linha,followComandos,lenMax);
					free(erro);
				}
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador', 'while', 'if' ou 'end' esperado.");
		lenMax=sizeof(followComandos)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followComandos,lenMax);
		free(erro);
	}
}
void trataComando(arvore *a)
{
	int lenMax=sizeof(firstComando)/sizeof(int);
	if(validaFirst(k->lexema,firstComando,lenMax))
	{
		if(k->lexema==IDENTIFICADOR)
		{
			arvore *b = criaArv(NOME, "NOME", k->linha);
			insereNo(a,b);
			trataNome(b);
			if (k) 
			{
				arvore *b = criaArv(ATRIBUICAO, "ATRIBUICAO", k->linha);
				insereNo(a,b);
				trataAtribuicao(b);
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
		else
		{
			if(k->lexema==WHILE)
			{
				arvore *b= criaArv(k->lexema,k->token, k->linha);
				insereNo(a,b);
				k=nextToken();//botei dps
				if(k)
				{
					arvore *b = criaArv(EXP_LOGICA, "EXP_LOGICA", k->linha);
					insereNo(a,b);
					trataExp_Logica(b);
					if(k)
					{
						arvore *b = criaArv(BLOCO, "BLOCO", k->linha);
						insereNo(a,b);
						trataBloco(b);
					}
					else
					{
						printf("Fim de arquivo inesperado!\n");
						exit(1);
					}
				}
				else
				{
					printf("Fim de arquivo inesperado!\n");
					exit(1);
				}
			}
			else
			{
				if(k->lexema==IF)
				{
					arvore *b= criaArv(k->lexema,k->token, k->linha);
					insereNo(a,b);
					k=nextToken();
					if(k)
					{
						b = criaArv(EXP_LOGICA, "EXP_LOGICA", k->linha);
						insereNo(a,b);
						trataExp_Logica(b);
						//k=nextToken(); 
						if(k->lexema==THEN)
						{
							b= criaArv(k->lexema,k->token, k->linha);
							insereNo(a,b);
							k=nextToken();//botei dps
							b = criaArv(BLOCO, "BLOCO", k->linha);
							insereNo(a,b);
							trataBloco(b);
						}
						else
						{
							char *erro=(char*)malloc(sizeof(char)*80);
							strcpy(erro,"'then' esperado.");
							lenMax=sizeof(followComando)/sizeof(int);
							trata_erro_sintatico(erro,k->linha,followComando,lenMax);
							free(erro);
						}
					}
					else
					{
						printf("Fim de arquivo inesperado!\n");
						exit(1);
					}
				}
				else
				{
					char *erro=(char*)malloc(sizeof(char)*80);
					strcpy(erro,"'if' ou 'while' esperado.");
					lenMax=sizeof(followComando)/sizeof(int);
					trata_erro_sintatico(erro,k->linha,followComando,lenMax);
					free(erro);
				}
			}
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador', 'while' ou 'if' esperado.");
		lenMax=sizeof(followComando)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followComando,lenMax);
		free(erro);
	}		
}
void trataAtribuicao(arvore *a)
{
	int lenMax=sizeof(firstAtribuicao)/sizeof(int);
	if(validaFirst(k->lexema,firstAtribuicao, lenMax))
	{
		if(k->lexema==ATRIBUICAO)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();//botei dps
			b = criaArv(VALOR, "VALOR", k->linha);
			insereNo(a,b);
			trataValor(b);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"':=' esperado.");
		lenMax=sizeof(followAtribuicao)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followAtribuicao,lenMax);
		free(erro);
	}
}
void trataValor(arvore *a)
{
	int lenMax=sizeof(firstValor)/sizeof(int);
	if(validaFirst(k->lexema,firstValor, lenMax))
	{
		if(k->lexema == NUMERO)
		{
			lenMax=sizeof(firstExp_Matematica)/sizeof(int);
			if(validaFirst(k->lexema,firstExp_Matematica,lenMax))
			{
				arvore *b = criaArv(EXP_MATEMATICA, "EXP_MATEMATICA", k->linha);
				insereNo(a,b);
				trataExp_Matematica(b);
			}
		}
		else
		{
			if(k->lexema == IDENTIFICADOR)
			{
				listatoken *l=k;
				l=l->proximo;
				int lenMax=sizeof(firstParametros)/sizeof(int);
				if(validaFirst(l->lexema, firstParametros, lenMax))
				{
					arvore *b = criaArv(IDENTIFICADOR, "IDENTIFICADOR", k->linha);
					insereNo(a,b);
					trataIdentificador(b);
					if(k)
					{
							arvore *b = criaArv(PARAMETROS, "PARAMETROS", k->linha);
							insereNo(a,b);
							trataParametros(b);
					}
					else
					{
							printf("Fim de arquivo inexperado!\n");
							exit(1);
					}
				}
				else
				{
					arvore *b = criaArv(EXP_MATEMATICA, "EXP_MATEMATICA", k->linha);
					insereNo(a,b);
					trataExp_Matematica(b);
				}
			}
			else
			{
				char *erro=(char*)malloc(sizeof(char)*80);
				strcpy(erro,"'identificador' esperado.");
				lenMax=sizeof(firstValor)/sizeof(int);
				trata_erro_sintatico(erro,k->linha,firstValor,lenMax);
				free(erro);
			}
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'numero' ou 'identificador' esperado.");
		lenMax=sizeof(followValor)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followValor,lenMax);
		free(erro);
	}
}
void trataParametros(arvore *a)
{
	int lenMax=sizeof(firstParametros)/sizeof(int);
	if(validaFirst(k->lexema,firstParametros,lenMax))
	{
		if(k->lexema==ABRE_PARENTESES)
		{
			arvore *b= criaArv(k->lexema,k->token, k->linha);
			insereNo(a,b);
			k=nextToken();//botei dps
			b = criaArv(LISTA_PARAM, "LISTA_PARAM", k->linha);
			insereNo(a,b);
			trataLista_Param(b);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
			strcpy(erro,"'(' esperado.");
			lenMax=sizeof(followParametros)/sizeof(int);
			trata_erro_sintatico(erro,k->linha,followParametros,lenMax);
			free(erro);
	}
}
void trataLista_Param(arvore *a)
{
	int lenMax=sizeof(firstLista_Var)/sizeof(int);
	if(validaFirst(k->lexema,firstLista_Param,lenMax))
	{
		arvore *b = criaArv(NOME_NUMERO, "NOME_NUMERO", k->linha);
		insereNo(a,b);
		trataNome_Numero(b);
		//k=nextToken();
		if (k) 
		{
			if(k->lexema== VIRGULA)
			{
				b= criaArv(k->lexema,k->token, k->linha);
				insereNo(a,b);
				k=nextToken();//botei dps
				b = criaArv(LISTA_PARAM, "LISTA_PARAM", k->linha);
				insereNo(a,b);
				trataLista_Param(b);					
			}
			if(k->lexema== FECHA_PARENTESES)
			{
				b= criaArv(k->lexema,k->token, k->linha);
				insereNo(a,b);
				k=nextToken();//botei dps
			}
		}	
		else
		{
			printf("Fim de arquivo inesperado!\n");
			exit(1);
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'numero' ou 'identificador' esperado.");
		lenMax=sizeof(followLista_Param)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followLista_Param,lenMax);
		free(erro);
	}

}
void trataExp_Logica(arvore *a)
{
	int lenMax=sizeof(firstExp_Logica)/sizeof(int);
	if(validaFirst(k->lexema,firstExp_Logica,lenMax))
	{
		arvore *b = criaArv(EXP_MATEMATICA, "EXP_MATEMATICA", k->linha);
		insereNo(a,b);
		trataExp_Matematica(b);
		//k=nextToken();
		lenMax=sizeof(firstOp_Relacional)/sizeof(int);
		if(validaFirst(k->lexema,firstOp_Relacional,lenMax))
		{
			//b= criaArv(k->lexema,k->token, k->linha);
			//insereNo(a,b);
			b = criaArv(OP_RELACIONAL, "OP_RELACIONAL", k->linha);
			insereNo(a,b);
			trataOp_Relacional(b);
			if(k)
			{
				//b= criaArv(k->lexema,k->token, k->linha);
				//insereNo(a,b);
				b = criaArv(EXP_LOGICA, "EXP_LOGICA", k->linha);
				insereNo(a,b);
				trataExp_Logica(b);

			}
			else
			{
				printf("Fim de arquivo inesperado\n");
				exit(1);
			}
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' ou 'numero' esperado.");
		lenMax=sizeof(followOp_Relacional)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followOp_Relacional,lenMax);
		free(erro);
	}
}
void trataOp_Relacional(arvore *a)
{
	int lenMax=sizeof(firstOp_Relacional)/sizeof(int);
	if (validaFirst(k->lexema, firstOp_Relacional,lenMax)) 
	{
        arvore *b = criaArv(k->lexema, k->token, k->linha);
        insereNo(a, b);
        k = nextToken();
    } 
	else 
	{
        char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"Operador relacional ou '=' esperado.");
		lenMax=sizeof(followOp_Relacional)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followOp_Relacional,lenMax);
		free(erro);
    }
}
void trataExp_Matematica(arvore *a)
{
	int lenMax=sizeof(firstExp_Matematica)/sizeof(int);
	if(validaFirst(k->lexema,firstExp_Matematica,lenMax))
	{
		arvore *b = criaArv(NOME_NUMERO, "NOME_NUMERO", k->linha);
		insereNo(a,b);
		trataNome_Numero(b);
		//k=nextToken();
		lenMax=sizeof(firstOp_Matematico)/sizeof(int);
		if(validaFirst(k->lexema,firstOp_Matematico,lenMax))
		{
			//b= criaArv(k->lexema,k->token, k->linha);
			//insereNo(a,b);
			b = criaArv(OP_MATEMATICO, "OP_MATEMATICO", k->linha);
			insereNo(a,b);
			trataOp_Matematico(b);
			if(k)
			{
				//b= criaArv(k->lexema,k->token, k->linha);
				//insereNo(a,b);
				b = criaArv(EXP_MATEMATICA, "EXP_MATEMATICA", k->linha);
				insereNo(a,b);
				trataExp_Matematica(b);

			}
			else
			{
				printf("Fim de arquivo inesperado\n");
				exit(1);
			}
		}
	}
	/*else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'numero' ou 'identificador' esperado.");
		lenMax=sizeof(followExp_Matematica)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followExp_Matematica,lenMax);
		free(erro);
	}*/
}
void trataOp_Matematico(arvore *a)
{
	int lenMax=sizeof(firstOp_Matematico)/sizeof(int);
	if (validaFirst(k->lexema, firstOp_Matematico,lenMax)) 
	{
        arvore *b = criaArv(k->lexema, k->token, k->linha);
        insereNo(a, b);
        k = nextToken();
    } 
	else 
	{
        char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"Operador matematico esperado.");
		lenMax=sizeof(followOp_Matematico)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followOp_Matematico,lenMax);
		free(erro);
    }
}
void trataNome_Numero(arvore *a)
{
	int lenMax=sizeof(firstNome_Numero)/sizeof(int);
	if(validaFirst(k->lexema,firstNome_Numero,lenMax))
	{
		lenMax=sizeof(firstNome)/sizeof(int);
		if(validaFirst(k->lexema,firstNome,lenMax))
		{
			arvore *b = criaArv(NOME, "NOME", k->linha);
			insereNo(a,b);
			trataNome(b);
		}
		else
		{
			if(k->lexema == NUMERO)
			{
				arvore *b = criaArv(NUMERO, "NUMERO", k->linha);
				insereNo(a,b);
				trataNumero(b);;
			}
			else
			{
				printf("Fim de arquivo inesperado!\n");
				exit(1);
			}
		}
	}
	else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'numero' ou 'identificador' esperado.");
		lenMax=sizeof(followNome_Numero)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followNome_Numero,lenMax);
		free(erro);
	}
}
void trataNome(arvore *a)
{
	int lenMax=sizeof(firstNome)/sizeof(int);
	if(validaFirst(k->lexema,firstNome,lenMax))
	{
		arvore *b;//= criaArv(k->lexema,k->token, k->linha);
		//insereNo(a,b);
		b = criaArv(IDENTIFICADOR, "IDENTIFICADOR", k->linha);
		insereNo(a,b);
		trataIdentificador(b);
		if(k)
		{
			if(k->lexema==ABRE_COLCHETE)
			{
				b= criaArv(k->lexema,k->token, k->linha);
				insereNo(a,b);
				k=nextToken();//botei dps
				b = criaArv(NOME_NUMERO, "NOME_NUMERO", k->linha);
				insereNo(a,b);
				trataNome_Numero(b);
				//k=nextToken();
				if(k)
				{
					if(k->lexema==FECHA_COLCHETE)
					{
						b= criaArv(k->lexema,k->token, k->linha);
						insereNo(a,b);
						k=nextToken();//botei dps
					}
					else
					{
						char *erro=(char*)malloc(sizeof(char)*80);
						strcpy(erro,"']' esperado.");
						lenMax=sizeof(firstNome)/sizeof(int);
						trata_erro_sintatico(erro,k->linha,firstNome,lenMax);
						free(erro);
					}
				}
				else
				{
					printf("Fim de arquivo inesperado!\n");
					exit(1);
				}
			}
		}
		else
		{
			printf("Fim de arquivo inesperado!\n");
			exit(1);
		}
	}
	/*else
	{
		char *erro=(char*)malloc(sizeof(char)*80);
		strcpy(erro,"'identificador' esperado.");
		lenMax=sizeof(followNome)/sizeof(int);
		trata_erro_sintatico(erro,k->linha,followNome,lenMax);
		free(erro);
	}*/
}

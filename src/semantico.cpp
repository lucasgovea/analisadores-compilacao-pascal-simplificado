#include "semantico.h"
#include <stdio.h>
#include <string.h>
#include "tabelasimbolos.h"
#include "lexico.h"

TabSimb *tabela = NULL;
void percorreFilhosVariaveis(arvore *a, char* tipo)
{
	if(a->irmao!=NULL)
	{
		if(strcmp(a->irmao->token,":")!=0)
		{
			percorreFilhosVariaveis(a->irmao,tipo);
		}
	}
	else
	{
		if(a->filho!=NULL)
		{	
			percorreFilhosVariaveis(a->filho, tipo);
		}
		else if(strcmp(a->token,",")!=0)
		{
			tabela=insereSimb(tabela,"",tipo,"","","",a->token,"");
		}
	}
	
	
}


void percorreVariavel(arvore *a)
{
	char tipo[30];
	arvore *b=a->filho;
	do
	{
		b=b->irmao;
	}while(b->irmao!=NULL);
	b=b->filho;
	strcpy(tipo,b->token);
	
	percorreFilhosVariaveis(a->filho->filho, tipo);
	
}

void percorreConstante(arvore *a)
{
	a=a->filho;
	a=a->filho;
	tabela=insereSimb(tabela,"funcoes","const","","","","TAM","");
}

void percorreTipo(arvore *a)
{
	
}

void percorreFuncao(arvore *a)
{

}



void semantico(arvore *a)
{
	arvore *b=NULL;

	if (a->lexema== TIPO)
	{
		percorreTipo(a);
    }
	else if (a->lexema == CONSTANTE)
	{
		percorreConstante(a);
    }
	else if (a->lexema== VARIAVEL)
	{
		percorreVariavel(a);
    }
	else if (a->lexema==FUNCAO)
	{
        percorreFuncao(a);
    }
	
	for (b = a->filho; b != NULL; b = b->irmao) {

        semantico(b);
    }
	
}
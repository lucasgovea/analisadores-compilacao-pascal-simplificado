#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "lexico.h"
#include "listalex.h"


//\ � COMENTARIO EM UMA LINHA
//E { } � COMENTARIO EM VARIAS LINHAS
char palavra;

void trata_erro(char *token, int linha)
{
	printf ("Erro na linha=%d -> Token: %s \n\n", linha, token);
}

char tokenSeguinte(FILE *arq)
{
	palavra=fgetc(arq);
	return palavra;
}

// isNumeric function. Returns 0 = if not numeric, !=0 if numeric.
int isNumeric(char* str)
{
    int len = strlen(str);          // get the length of string for iterations
    int i = 0;                      // iterator
    int ret = 1;                    // for the return type
    int deccnt = 0;                 // decimal counter
    while(i < len && ret != 0)
    {
        if(str[i] == '.')           // is there a decimal
        {
            deccnt++;               // count a decimal
            if(deccnt > 1)          // is there too many decimal points
                ret = 0;            // too many decimals set return for not numeric
        }
        else
            ret = isdigit(str[i]); // is this character numeric
			i++;                       // increment to next character
    }
    return ret;                    // return result =0 not numeric !=0 is numeric
}

// isIntFloat function. Returns -1 = if float, 1 if int, 0 if no number.
int isIntFloat(char* str)
{
    int i=isNumeric(str);
    int j=0;
    int ret=0, cont=0;
    if(i!=0)
    {
        for(j=0;j<strlen(str);j++)
        {
            if(str[j]=='.')
            {
                cont++;
            }
        }
        if(cont==1)
        {
            ret = -1; //float
        }
        else
        {
            ret = 1; //int
        }

    }
    else
    {
        ret= 0; //nao � numero
    }
    return ret;
}



listatoken *lexico(char nomearq[100])
{

    FILE *arq=NULL;
    char token_aux[2];
    char token_aux2[4];
    char *token ;  //token que vai ser adicionado na lista
    int linha=1;                //linha onde ocorreu o erro
	char aux;
    listatoken *lista = NULL;


    arq = fopen(nomearq, "rt");
    if(arq==NULL){
        printf("\nErro na abertura do arquivo!\n");
        return 0;
    }

    while((palavra = tokenSeguinte(arq)) != EOF)
    {

        //digito
		if(isdigit(palavra))
		{

			token = (char*)calloc(30, sizeof (char));
			do{
				token_aux[0] = palavra;
				token_aux[1] = '\0';
				strcat(token, token_aux);
				palavra = tokenSeguinte(arq);

			}while(isdigit(palavra) || palavra=='.');



			if(isNumeric(token))
			{
				if(isIntFloat(token)==1)
				{
					lista = insere_elem(token,NUMERO,linha,lista); //nao � identificador, eh numero
				}
				if(isIntFloat(token)==-1)
				{
					lista = insere_elem(token,NUMERO,linha,lista);
				}
			}
			else
			{
				lista = insere_elem(token,ERRO,linha,lista);
				trata_erro(token,linha);
			}

		}


        //letra
		if(isalpha(palavra))
		{
			token = (char*)calloc(30, sizeof (char));
		    do
		    {
                    token_aux[0] = palavra;
                    token_aux[1] = '\0';
                    strcat(token, token_aux);
                    palavra = tokenSeguinte(arq);

            }while (isalpha(palavra));

			if(isdigit(palavra))
			{
				do{
					token_aux[0] = palavra;
                    token_aux[1] = '\0';
                    strcat(token, token_aux);
                    palavra = tokenSeguinte(arq);
				}while(isdigit(palavra));

			}

            if(!stricmp(token, "program")
			   ||(!stricmp(token, "of"))
               ||(!stricmp(token, "var"))
               ||(!stricmp(token, "integer"))
               ||(!stricmp(token, "real"))
               ||(!stricmp(token, "function"))
               ||(!stricmp(token, "type"))
               ||(!stricmp(token, "const"))
               ||(!stricmp(token, "array"))
               ||(!stricmp(token, "begin"))
               ||(!stricmp(token, "end"))
               ||(!stricmp(token, "while"))
               ||(!stricmp(token, "if"))
               ||(!stricmp(token, "then")))
            {
                if(!stricmp(token, "program"))
                {
                    lista = insere_elem(token,PROGRAM,linha,lista);
                }
				if(!stricmp(token, "of"))
				{
					lista = insere_elem(token,OF,linha,lista);
				}
                if(!stricmp(token, "var"))
                {
                    lista = insere_elem(token,VAR,linha,lista);
                }
                if(!stricmp(token, "integer"))
                {
                    lista = insere_elem(token,TIPO,linha,lista);
                }
                if(!stricmp(token, "real"))
                {
                    lista = insere_elem(token,TIPO,linha,lista);
                }
                if(!stricmp(token, "function"))
                {
                    lista = insere_elem(token,FUNCTION,linha,lista);
                }
                if(!stricmp(token, "type"))
                {
                    lista = insere_elem(token,TYPE,linha,lista);
                }
                if(!stricmp(token, "const"))
                {
                    lista = insere_elem(token,CONST,linha,lista);
                }
                if(!stricmp(token, "array"))
                {
                    lista = insere_elem(token,TIPO,linha,lista);
                }
                if(!stricmp(token, "begin"))
                {
                    lista = insere_elem(token,BEGIN,linha,lista);
                }
                if(!stricmp(token, "end"))
                {
                    lista = insere_elem(token,END,linha,lista);
                }
                if(!stricmp(token, "while"))
                {
                    lista = insere_elem(token,WHILE,linha,lista);
                }
                if(!stricmp(token, "if"))
                {
                    lista = insere_elem(token,IF,linha,lista);
                }
                if(!stricmp(token, "then"))
                {
                    lista = insere_elem(token,THEN,linha,lista);
                }
            }
            else
            {
                 lista = insere_elem(token,IDENTIFICADOR,linha,lista);
            }

		}

        //tratar casos especiais ; , ( ) [ ] : < > = ! / * - +
        if(ispunct(palavra))
        {

                token_aux[0] = palavra;
                token_aux[1] = '\0';
                if((!strcmp(token_aux, "+"))
                    || (!strcmp(token_aux, "-"))
                    || (!strcmp(token_aux, "/"))
                    || (!strcmp(token_aux, "*"))
                    || (!strcmp(token_aux, "("))
                    || (!strcmp(token_aux, ")"))
                    || (!strcmp(token_aux, "["))
                    || (!strcmp(token_aux, "]"))
                    || (!strcmp(token_aux, ";"))
                    || (!strcmp(token_aux, ","))
                    || (!strcmp(token_aux, "!"))
                    || (!strcmp(token_aux, "<"))
                    || (!strcmp(token_aux, ">"))
                    || (!strcmp(token_aux, "="))
                    || (!strcmp(token_aux, ":")))
                {

                    //operadores matematicos
                    if((!strcmp(token_aux, "+"))
                      || (!strcmp(token_aux, "-"))
                      || (!strcmp(token_aux, "/"))
                      || (!strcmp(token_aux, "*")))
                    {
                        lista=insere_elem(token_aux,OP_MATEMATICO, linha, lista);
                    }

                    //operadores logicos
                    if((!strcmp(token_aux, "<"))
                      || (!strcmp(token_aux, ">"))
                      || (!strcmp(token_aux, "!")))
                    {
                        lista=insere_elem(token_aux,OP_RELACIONAL, linha, lista);
                    }
					if(!strcmp(token_aux, "="))
					{
						lista=insere_elem(token_aux,IGUAL, linha, lista);
					}

                    //(dois ponto igual) :=
                    if (!strcmp(token_aux, ":")) {
                        strcpy(token_aux2, token_aux);

                        palavra = tokenSeguinte(arq);
						token_aux[0] = palavra;
                        token_aux[1] = '\0';
                        if (!strcmp(token_aux, "=")) {
                            strcat(token_aux2, token_aux);
                            lista= insere_elem(token_aux2, ATRIBUICAO, linha,lista);
                        } else {
							fseek(arq, -1*sizeof(char), SEEK_CUR);
                            lista = insere_elem(token_aux2, DOIS_PONTOS, linha,lista);

                        }
                    }

                    //outros casos ; , ( ) [ ]
                    if (!strcmp(token_aux, ";"))
                    {
                        lista = insere_elem(token_aux,PONTO_VIRGULA,linha,lista);
                    }

                    if (!strcmp(token_aux, ","))
                    {
                        lista = insere_elem(token_aux,VIRGULA,linha,lista);
                    }

                    if (!strcmp(token_aux, "("))
                    {
                        lista = insere_elem(token_aux,ABRE_PARENTESES,linha,lista);
                    }

                    if (!strcmp(token_aux, ")"))
                    {
                        lista = insere_elem(token_aux,FECHA_PARENTESES,linha,lista);
                    }

                    if (!strcmp(token_aux, "["))
                    {
                        lista = insere_elem(token_aux,ABRE_COLCHETE,linha,lista);
                    }

                    if (!strcmp(token_aux, "]"))
                    {
                        lista = insere_elem(token_aux,FECHA_COLCHETE,linha,lista);
                    }


                }
                else
                {
					//tratemento de comentario { }
					if (!strcmp(token_aux, "{"))
					{

						  do
						  {
							  aux = fgetc(arq);
							  if(aux == '\n')
							  {
								  linha++;
							  }

						  }while(aux!= '}');
					}

					//tratamento de comentario /
					else if(!strcmp(token_aux, "\\"))
					{
						do
						{
							 aux=fgetc(arq);

						}while(aux != '\n');
						linha++;
					}
					else
					{
						lista = insere_elem(token_aux,ERRO,linha,lista);
						trata_erro(token_aux,linha);
					}

				}



        }

		//pula linha
		if (palavra=='\n'){
			linha++;
		}

    }
    free(token);
	fclose(arq);
	lista = insere_elem("END_OF_FILE",END_OF_FILE,-1,lista);
    return lista;
}





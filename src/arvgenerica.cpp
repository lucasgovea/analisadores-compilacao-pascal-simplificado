#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvgenerica.h"
#include "tabelasimbolos.h"

arvore *criaArv(int lexema, char *token, int linha) 
{
    arvore *a = (arvore*) malloc(sizeof (arvore));
    a->lexema = lexema;
    a->token = (char*)malloc(30 * sizeof (char));
    strcpy(a->token,token);
    a->linha = linha;
    a->filho = a->irmao = NULL;
    return a;
}

void insereNo(arvore *a, arvore *suba) 
{
    if (!a->filho) 
	{
        suba->irmao = a->filho;
        a->filho = suba;
    } 
	else 
	{
        arvore *p;
        p = a->filho;
        while (p->irmao) 
		{
            p = p->irmao;
        }
        p->irmao = suba;
    }
}

void imprimeArv(arvore *a, int n) 
{
	int i,j;
    if (!a)
	{
        printf("null!");
    }
    arvore *p;
	for(i=0;i<n;i++)
			printf(" ");
	printf("<%s\n", a->token);
    for (p = a->filho; p != NULL; p = p->irmao) 
	{
		if(n%20==0)
		system("pause");
        imprimeArv(p,++n);
		
		for(j=n;j>0;j--)
			printf(" ");
		printf(">\n");
		
    }

}

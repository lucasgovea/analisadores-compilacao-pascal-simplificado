#ifndef LEXICO_H
#define LEXICO_H
#include "listalex.h"

#define ERRO 1
#define IDENTIFICADOR 2 //palavra variavel
#define PROGRAM 3
#define OP_RELACIONAL 4
#define OP_MATEMATICO 5
//#define ARRAY 6
#define FUNCTION 7
#define BEGIN 8
#define END 9
#define IF 10
#define THEN 11
#define ABRE_PARENTESES 12
#define FECHA_PARENTESES 13
#define VAR 14
#define TYPE 15
#define ABRE_COLCHETE 16
#define FECHA_COLCHETE 17
#define PONTO_VIRGULA 18
#define ATRIBUICAO 19
#define WHILE 20
#define DOIS_PONTOS 21
#define VIRGULA 22
#define CONST 23
#define OF 24
#define IGUAL 25
#define EPSILON 26
#define END_OF_FILE 27
#define TIPO 28
#define NUMERO 29



listatoken *lexico(char nomearq[30]);
void trata_erro(char token[2], int linha);

#endif

#ifndef LISTALEX_H
#define LISTALEX_H



typedef struct tokenlist{
    char token [30];
    int lexema ; //classificaçao do token
	struct tokenlist *proximo;
	int linha;
}listatoken;


listatoken *insere_elem(char *novotoken, int novolexema, int novolinha, listatoken *l);
void imprime_listatoken(listatoken *t);


#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listalex.h"



listatoken *novo_elem(){
    listatoken *t = (listatoken *) malloc (sizeof (listatoken));
	t -> proximo = NULL;
	return t;
}

listatoken *insere_elem(char *novotoken, int novolexema, int novolinha, listatoken *l){
    listatoken *p,*aux = NULL;
    if(l==NULL){
        l=(listatoken *) malloc (sizeof (listatoken));
        strcpy(l->token, novotoken);
        l->lexema = novolexema;
        l->linha=novolinha;
        l->proximo=NULL;
    }
    else{
        p=(listatoken *) malloc (sizeof (listatoken));
        strcpy(p->token, novotoken);
        p->lexema = novolexema;
        p->linha=novolinha;
        p->proximo=NULL;
		
		aux=l;
		while(aux->proximo!=NULL){
			aux=aux->proximo;
		}
		aux->proximo=p;
    }
    return l;


}

void imprime_listatoken(listatoken *t)
{
    listatoken *aux = NULL;
	for (aux = t; aux != NULL; aux = aux -> proximo)
	{
		printf ("[token=%s linha=%d lexema=%d ]  ->  \n\n", aux -> token, aux -> linha, aux->lexema);
	}
}

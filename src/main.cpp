#include <stdio.h>
#include <stdlib.h>
#include "lexico.h"
#include "listalex.h"
#include "sintatico.h"
#include "arvgenerica.h"
#include "semantico.h"


int main(){

    char nomearq[100];//="programaexemplo.ex";
    listatoken *lista = NULL;
    printf("digite o nome do arquivo (caminho absoluto):");
    scanf("%s", nomearq);
    lista = lexico(nomearq);
    //imprime_listatoken(lista);
	printf("Fim do analisador lexico\n\n\n");
	printf("Inicio do analisador sintatico\n");
	arvore *a = sintatico(lista);
	//imprimeArv(a,0);
	printf("Fim do analisador sintatico\n\n\n");
	printf("Inicio do analisador semantico\n");
	semantico(a);
	printf("Fim do analisador semantico\n\n\n");
	system("pause");
    return 0;
}
